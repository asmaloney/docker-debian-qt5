FROM debian:sid-slim
MAINTAINER asmaloney@gmail.com

ENV DEBIAN_FRONTEND noninteractive

RUN apt-get -y update && \
  apt-get -y install \
    g++ \
    make \
    qt5-qmake \
    qt5-default \
    qt3d5-dev \
    libqt53dextras5 \
    qttools5-dev \
    qtwebengine5-dev \
    qtscript5-dev \
    libqt5svg5-dev \
    && \
  apt-get clean && \
  rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* /usr/share/man/?? /usr/share/man/??_*
